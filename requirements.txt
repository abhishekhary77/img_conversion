Django==2.1
djangorestframework==3.9.4
djangorestframework-simplejwt==4.3.0
# mysqlclient==1.3.14
# torchvision
#numpy==1.18.1
#opencv-python==4.1.2.30
# Pillow==7.0.0
# scipy==1.4.1
# torch==0.3.1
# django-taggit==1.1.0
# django-taggit-serializer==0.1.7
# django-s3-storage==0.12.4
# social-auth-app-django==3.1.0
# social-auth-core==3.2.0
# rest-social-auth==2.2.0
