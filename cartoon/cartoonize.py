#import pillow
import sys
import PIL
import numpy as np
from base64 import b64encode, b64decode
from io import BytesIO
import cv2
import cartoongan

class Arguements():

    def __init__(self):
        self.debug=False
        self.keep_original_size=False
        self.max_resized_height=300 
        self.show_tf_cpp_log=False 
        self.styles=['shinkai', 'hayao', 'hosoda', 'paprika']
        #self.styles='paprika'

args = Arguements()


def pre_processing(imgdata, style, expand_dim=True):

    input_image = PIL.Image.open(BytesIO(imgdata)).convert("RGB")

    if not args.keep_original_size:
        width, height = input_image.size
        aspect_ratio = width / height
        resized_height = min(height, args.max_resized_height)
        resized_width = int(resized_height * aspect_ratio)
        if width != resized_width:
            input_image = input_image.resize((resized_width, resized_height))

    input_image = np.asarray(input_image)
    input_image = input_image.astype(np.float32)

    input_image = input_image[:, :, [2, 1, 0]]

    if expand_dim:
        input_image = np.expand_dims(input_image, axis=0)
    return input_image


def post_processing(transformed_image, style):
    if not type(transformed_image) == np.ndarray:
        transformed_image = transformed_image.numpy()
    transformed_image = transformed_image[0]
    transformed_image = transformed_image[:, :, [2, 1, 0]]
    transformed_image = transformed_image * 0.5 + 0.5
    transformed_image = transformed_image * 255
    return transformed_image


def save_transformed_image(output_image):
  
    if output_image is not None:
        image = PIL.Image.fromarray(output_image.astype("uint8"))
        opencvImage = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
        gray = cv2.cvtColor(opencvImage, cv2.COLOR_BGR2GRAY)
        gray = cv2.medianBlur(gray, 5)
        edge1 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 5, 5)
        edge1 = cv2.bitwise_not(edge1)
        contours, _ = cv2.findContours(edge1,
                                   cv2.RETR_EXTERNAL,
                                   cv2.CHAIN_APPROX_NONE)

        cv2.drawContours(opencvImage, contours, -1, 0, thickness=1)
        is_success, buffer = cv2.imencode(".png", opencvImage)
        outputIoStream = BytesIO(buffer)
        return b64encode(outputIoStream.getvalue())


def main(image, style):

    # decide what styles to used in this execution
    
    styles = args.styles
    if style in styles:
    
        model = cartoongan.load_model(style)

        imgdata = b64decode(str(image))


        input_image = pre_processing(imgdata, style=style)
    
        transformed_image = model(input_image)
        output_image = post_processing(transformed_image, style=style)
        transformed_image_b64 = save_transformed_image(output_image)
        return transformed_image_b64
