from .cartoonize import main
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


class Cartoon(APIView):

    permission_classes = ()

    def post(self, request):
        image = request.data['image']
        style = request.data.get('style', 'shinkai')
        image_cov = main(image, style)
        body = {
        "input": image_cov}
        return Response(body)