from django.urls import path, include
# from django.contrib import admin
# from rest_framework_simplejwt import views as jwt_views
from django.conf import settings
from django.conf.urls.static import static
from cartoon.views import Cartoon
urlpatterns = [
    path('', Cartoon.as_view(), name="cartoon"),
    # path('', include('users.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
 