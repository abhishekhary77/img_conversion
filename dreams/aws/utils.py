from storages.backends.s3boto3 import S3Boto3Storage

StaticRootS3BotoStorage = lambda: S3Boto3Storage(location='static')
# MediaRootS3BotoStorage  = lambda: S3Boto3Storage(location='media')

class MediaRootS3BotoStorage(S3Boto3Storage):
    location = 'media'

    def _get_security_token(self):
        return None